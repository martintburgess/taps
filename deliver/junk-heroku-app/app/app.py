import pandas as pd
from flask import Flask, render_template

# from tornado.httpserver import HTTPServer
# from bokeh.server.server import BaseServer
# from bokeh.server.tornado import BokehTornado
# from bokeh.server.util import bind_sockets

from bokeh.embed import server_document
from bokeh.plotting import figure
from bokeh.server.server import Server
from bokeh.themes import Theme
from tornado.ioloop import IOLoop

from bokeh.models import Title
from bokeh.models.map_plots import GMapPlot, GMapOptions
from bokeh.models.tools import PanTool, WheelZoomTool, ResetTool, HoverTool
from bokeh.models.mappers import CategoricalColorMapper
from bokeh.models.sources import ColumnDataSource
from bokeh.models.markers import Circle
from bokeh.models.ranges import Range1d
from bokeh.application.handlers import FunctionHandler
from bokeh.application import Application
from bokeh.io import show, output_notebook, curdoc
from bokeh.layouts import widgetbox, column
from bokeh.models.widgets import CheckboxButtonGroup, Slider, Button, Dropdown

size = pd.read_csv('size.csv', index_col=0, parse_dates=True)
colour = pd.read_csv('colour.csv', index_col=0, parse_dates=True)
locations = pd.read_csv('locations.csv', index_col=0, parse_dates=True)

app = Flask(__name__)

# Create the Document Application
def modify_doc(doc):

    ## Create the main plot
    # prepare initial plot data (we'll start with buses)
    s = size.loc['2016-11-21', list(locations[locations['tmode_idx'].isin([0])].index.values)]
    c = colour.loc['2016-11-21', list(locations[locations['tmode_idx'].isin([0])].index.values)]
    l = locations[locations['tmode_idx'].isin([0])]
    source = ColumnDataSource(data=dict(
                    lat=l.latitude.tolist(),
                    lon=l.longitude.tolist(),
                    col = c.iloc[0].tolist(),
                    siz = s.iloc[0].tolist(),
                    desc=s.columns.tolist()))

    # prepare map
    map_options = GMapOptions(lat=-33.8688, lng=151.02366, map_type="roadmap", zoom=10)
    plot = GMapPlot(x_range=Range1d(), y_range=Range1d(), map_options=map_options, api_key='AIzaSyBbZEJWaGqbmrYqGAnYiyBg7uuFOvZQn1E')

    # prepare colour map
    color_mapper = CategoricalColorMapper(palette=["red","white","green"], factors=['neg','zero','pos'])

    # add a title
    tit = Title()
    tit.text = str(s.index[0].strftime("%I:%M %p"))
    tit.text_font_size = '40pt'
    plot.title = tit

    # Add circles
    circle = Circle(x="lon", y="lat", size="siz", fill_color={'field': 'col', 'transform': color_mapper}, fill_alpha=0.7, line_color=None)
    plot.add_glyph(source, circle)

    # Add some tools
    plot.add_tools(PanTool(), WheelZoomTool(), ResetTool(), HoverTool(tooltips=[("Location","@desc")]))

    ## Update plot
    def update_plot():

        if day_dropdown.value is None:
            day_dropdown.value = '2016-11-21'

        s = size.loc[day_dropdown.value, list(locations[locations['tmode_idx'].isin(tmode_selector.active)].index.values)]
        c = colour.loc[day_dropdown.value, list(locations[locations['tmode_idx'].isin(tmode_selector.active)].index.values)]
        l = locations[locations['tmode_idx'].isin(tmode_selector.active)]

        ti = int(time_slider.value)
        tit.text = str(s.index[ti].strftime("%-I:%M %p"))

        source.data=dict(
                    lat= l.latitude.tolist(),
                    lon= l.longitude.tolist(),
                    col = c.iloc[ti].tolist(),
                    siz = s.iloc[ti].tolist(),
                    desc = s.columns.tolist())


    ##  Controls
    # Select transport mode
    tmode_selector = CheckboxButtonGroup(labels=['Train','Bus','Ferry','Lightrail'], active=[0])

    def tmode_handler(attr, old, new):
        update_plot()

    tmode_selector.on_change("active",tmode_handler)

    # Time slider
    time_slider = Slider(start=0, end=95, value=0, step=1, title=None)

    def time_slider_handler(attrname, old, new):
        # update which time points are used

        update_plot()

    time_slider.on_change('value', time_slider_handler)

    # Day of the week selector
    menu = [('Monday', '2016-11-21'), ('Tuesday', '2016-11-22'), ('Wednesday', '2016-11-23'), ('Thursday', '2016-11-24'),('Friday', '2016-11-25'),('Saturday', '2016-11-26'), ('Sunday', '2016-11-27')]
    day_dropdown = Dropdown(label="Monday", button_type="default", menu = menu)

    def day_dropdown_handler(attrname, old, new):
        # update which time points are used
        day_dropdown.label = str(pd.to_datetime(new).weekday_name)
        update_plot()


    day_dropdown.on_change('value', day_dropdown_handler)

    # Animation
    def animate_update():
        new_time = time_slider.value + 1
        if new_time > 95:
            new_time = 0
        time_slider.value = new_time

    button = Button(label='► Play', width=60)

    callback_id = None

    def animate():
        global callback_id
        if button.label == '► Play':
            button.label = '❚❚ Pause'
            callback_id = curdoc().add_periodic_callback(animate_update, 400)
        else:
            button.label = '► Play'
            curdoc().remove_periodic_callback(callback_id)

    button.on_click(animate)

    controls = widgetbox([tmode_selector, button, time_slider,day_dropdown], sizing_mode='scale_width')
    #p = create_figure()
    layout = column(controls, plot)
    doc.add_root(layout)

    doc.theme = Theme(filename="theme.yaml")

# can't use shortcuts here, since we are pass to low level BokehTornado
tapmap = Application(FunctionHandler(modify_doc))

bokeh_tornado = BokehTornado({'/tapmap': tapmap}, extra_websocket_origins=["localhost:8000"])
bokeh_http = HTTPServer(bokeh_tornado)

# If this app is run using something like "gunicorn -w 4" then each process will listen on its own port
sockets, port = bind_sockets("localhost", 0)
bokeh_http.add_sockets(sockets)

@app.route('/', methods=['GET'])
def tapmap_page():
    script = server_document('http://localhost:%d/tapmap' % port)
    return render_template("index.html", script=script, template="Flask")


def bk_worker():
    # Can't pass num_procs > 1 in this configuration. If you need to run multiple
    # processes, see e.g. flask_gunicorn_embed.py
    io_loop = IOLoop.current()
    server = BaseServer(io_loop, bokeh_tornado, bokeh_http)
    server.start()
    server.io_loop.start()

from threading import Thread
Thread(target=bk_worker).start()

if __name__ == '__main__':
  app.run(port=33507)
# if __name__ == '__main__':
#     print('Opening single process Flask app with embedded Bokeh application on http://localhost:8001/')
#     print()
#     print('Multiple connections may block the Bokeh app in this configuration!')
#     print('See "flask_gunicorn_embed.py" for one way to run multi-process')
#     app.run(port=8001, debug=True)
